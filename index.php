<?php
include '../murt/App/Murt.php';
include '../murt/App/Vendors/RedBean/rb.php';
include '../murt/App/Vendors/RainTpl/inc/rain.tpl.class.php';
include './App/Models/Text.php';

// // include extra config:
// include 'App/config.php';

// Murt::$base = '/';
// OR:
// Murt::$base = '/other/path';
Murt::$base = '/p';
// OR dinamically:
// Murt::$base = dirname($_SERVER['SCRIPT_NAME']);

$routes = array(
    '/' => 'Main',
//    '/test/(\w+)' => 'Test',
    '/(\d+)' => 'View',
    '/r/(\d+)' => 'Raw',
);

Murt::init($routes, $_SERVER);
