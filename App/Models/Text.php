<?php
class Text {
    public static function save($data) {
       Murt::db();
       $text = R::dispense('texts');

       $text->title       = $data['title'      ];
       $text->author      = $data['author'     ];
       $text->private_key = $data['private_key'];
       $text->type        = $data['type'       ];
       $text->text        = $data['text'       ];
       $id = R::store($text);
       return $id;
    }
}
