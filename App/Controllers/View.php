<?php
class View {
    public static function GET($vars) {
        Murt::db();
        $text = R::load('texts', $vars[0]);
        if (!$text['id'])
            Murt::redir('/');
        // SyntaxHighliter < fix
        $text->text = str_replace('<', '&lt;', $text->text);
        Murt::render('', array('text' => $text));
    }
}
